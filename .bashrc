# if not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# aliases/variables/configuration only needed/wanted in interactive Bash sessions below

export EDITOR=vim
# sometimes takes precedence over EDITOR
export VISUAL="$EDITOR"

# don't put duplicate lines or lines starting with space in the history
# see bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS
shopt -s checkwinsize

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# we have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    if [ "$LOGNAME" = root ] || [ "`id -u`" -eq 0 ] ; then
        # obviously this only works if you set this file as the system-wide bash.bashrc under /etc
        PS1='\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[01;34m\]#\033[00m\] '
    else
        PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
    fi
else
    PS1='\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
fi

alias dog='less -FX'
alias gits='git status'

option_e=false # flaming helicopters fall from the sky

function boom_baby() {
    if [ "$LOGNAME" != root ] && [ "`id -u`" -ne 0 ] ; then
        (aplay /etc/3.1_4-root\@kali.wav --quiet --duration=97 &)
    fi
}

function sudo() {
    if [[ "$*" == "su" ]] && $option_e; then
        boom_baby
    fi
    command sudo "$@"
}

function su() {
    if [[ "$*" == "root" ]] && $option_e; then
        boom_baby
    fi
    command su "$@"
}
