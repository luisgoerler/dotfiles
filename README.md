# Dotfiles

This is a place for me to store configuration files of tools I use a lot. Also am planning on keeping them updated (by pushing any changes that generally make sense regarding the base-setup to main/pushing to a separate branch in case of them being specific to a certain type of setup). Having this is gonna make the setup of new machines easier
