" how to edit a vimrc file?
" type the following in Vim:
" :e $HOME/.vimrc (on UNIX/Mac)
" :e $HOME/_vimrc (on Windows)

set clipboard=unnamed
set noundofile
set nobackup nowritebackup
syntax on
set tabstop=4 shiftwidth=4 expandtab
